<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\DBConnector;

//require __DIR__.'/../vendor/autoload.php';

class DefaultController
{
    /**
     * @Route("/hello/{name}")
     */
     public function index($name) {
         return new Response(json_encode("Hi $name"));
     }

     /**
     * @Route("/api/register_new_time")
     */
    public function index2(Request $request) {
        //include('App/utils/DBConnector.php');
        $myconnection = new DBConnector();
        $param = json_encode($myconnection->getData());
        return new Response("Oh yeah! > ".$param);
    }
}