<?php
// src/utils/DBConnector.php
namespace App\Utils;

use \PDO;

class DBConnector
{
    private $data ;

    function __construct()
    {
        global $data ;
        $data = new PDO("mysql:host=localhost;dbname=symfotest","root","");
    }

    public function getData()
    {
        global $data;
        $ext = $data->prepare("Select * from usertime");
        $ext->execute();
        $data = $ext->fetchAll(PDO::FETCH_OBJ);
        return $data;
    }
}